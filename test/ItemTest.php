<?php

namespace App;

class ItemTest extends \PHPUnit\Framework\TestCase {

  public function testItem() {

    $item = new Item('+5 Dexterity Vest', 10, 20);

    $this->assertObjectHasAttribute("name", $item);
    $this->assertObjectHasAttribute("sell_in", $item);
    $this->assertObjectHasAttribute("quality", $item);

    $this->assertIsString($item->name);
    $this->assertIsNumeric($item->sell_in);
    $this->assertIsNumeric($item->quality);

    $text = $item->__toString();
    $this->assertEquals("+5 Dexterity Vest, 10, 20", $text);
  }

}