<?php

namespace App;

class CommonItemTest extends \PHPUnit\Framework\TestCase {

  public function testCreateItem() {

    $item = new Item('+5 Dexterity Vest', 10, 20);
    $commonItem = new CommonItem($item);
    $this->assertObjectHasAttribute("name", $commonItem);
    $this->assertObjectHasAttribute("sell_in", $commonItem);
    $this->assertObjectHasAttribute("quality", $commonItem);

    $this->assertIsString($commonItem->name);
    $this->assertIsNumeric($commonItem->sell_in);
    $this->assertIsNumeric($commonItem->quality);

    return $commonItem;
  }

  /**
   *
   * @depends testCreateItem
   */
  public function testUpdateQuality ($commonItem) {
    $commonItem->updateQuality();
    $this->assertEquals(9, $commonItem->sell_in);
    $this->assertEquals(19, $commonItem->quality);
  }
}