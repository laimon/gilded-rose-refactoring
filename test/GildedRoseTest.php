<?php

namespace App;

class GildedRoseTest extends \PHPUnit\Framework\TestCase {

    public function testCommonItem() {
      $item = new Item('+5 Dexterity Vest', 10, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(19, $gildedRoseItems[0]->quality);

    }

    public function testCommonSellInZeroItem() {
      $item = new Item('+5 Dexterity Vest', 0, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(-1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(18, $gildedRoseItems[0]->quality);

    }

    public function testCommonQualityZeroItem() {
      $item = new Item('+5 Dexterity Vest', 10, 0);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(0, $gildedRoseItems[0]->quality);

    }

    public function testAgedBrieItem() {
      $item = new Item('Aged Brie', 2, 0);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(1, $gildedRoseItems[0]->quality);
    }

    public function testAgedBrieSellInZeroItem() {
      $item = new Item('Aged Brie', 0, 0);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(-1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(2, $gildedRoseItems[0]->quality);
    }

    public function testAgedBrieMaxQualityItem() {
      $item = new Item('Aged Brie', 0, 50);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(-1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testSulfurasItem() {
      $item = new Item('Sulfuras, Hand of Ragnaros', 0, 80);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(0, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(80, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(14, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(21, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessElevenDaysItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(22, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessSixDaysItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 5, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(4, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(23, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassSellInZeroItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 0, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(-1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(0, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassMaxQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 15, 50);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(14, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessElevenDaysMaxQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, 50);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessSixDaysMaxQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 5, 50);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(4, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassNearQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 15, 49);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(14, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessElevenDaysNearQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testBackstagePassLessSixDaysNearQualityItem() {
      $item = new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(4, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(50, $gildedRoseItems[0]->quality);
    }

    public function testConjuredItem() {
      $item = new Item('Conjured Mana Cake', 10, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(18, $gildedRoseItems[0]->quality);

    }

    public function testConjuredSellInZeroItem() {
      $item = new Item('Conjured Mana Cake', 0, 20);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(-1, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(16, $gildedRoseItems[0]->quality);

    }

    public function testConjuredQualityZeroItem() {
      $item = new Item('Conjured Mana Cake', 10, 0);

      $items = [$item];

      $gildedRose = new GildedRose($items);
      $gildedRose->updateQuality();

      $gildedRoseItems = $gildedRose->getItems();

      $this->assertEquals(9, $gildedRoseItems[0]->sell_in);
      $this->assertEquals(0, $gildedRoseItems[0]->quality);

    }

}
