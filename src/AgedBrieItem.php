<?php

namespace App;

class AgedBrieItem extends ItemTemplate implements ItemInterface {
  public function updateQuality() {
    if ($this->quality < 50) {
      $this->quality++;
    }
    $this->sell_in--;
    if ($this->sell_in < 0 && $this->quality < 50) {
      $this->quality++;
    }
  }
}