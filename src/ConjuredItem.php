<?php

namespace App;

class ConjuredItem extends ItemTemplate implements ItemInterface {

  public function updateQuality() {
    if ($this->quality > 0) {
      $this->quality = $this->quality - 2;
    }
    $this->sell_in--;
    if ($this->sell_in < 0 && $this->quality > 0) {
      $this->quality = $this->quality - 2;
    }
  }
}