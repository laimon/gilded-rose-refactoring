<?php

namespace App;

final class GildedRose {

    private $items = [];

    public function __construct($items) {
        $this->setItems($items);
    }

    public function updateQuality() {
      foreach ($this->items as $item) {
        $item->updateQuality();
      }
    }

    public function setItems(array $items) {
      $updatedItems = [];
      foreach ($items as $key => $item) {
        switch($item->name) {
          case "Aged Brie":
            $updatedItems[$key] = new AgedBrieItem($item);
            break;
          case "Sulfuras, Hand of Ragnaros":
            $updatedItems[$key] = new SulfurasLegendaryItem($item);
            break;
          case "Backstage passes to a TAFKAL80ETC concert":
            $updatedItems[$key] = new BackstagePassItem($item);
            break;
          default:
            if (strpos($item->name, "Conjured") === 0) {
              $updatedItems[$key] = new ConjuredItem($item);
            } else {
              $updatedItems[$key] = new CommonItem($item);
            }
            break;
        }
      }
      $this->items = $updatedItems;
    }

    public function getItems() {
      return $this->items;
    }
}

