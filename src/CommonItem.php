<?php

namespace App;

class CommonItem extends ItemTemplate implements ItemInterface {
  public function updateQuality() {
    if ($this->quality > 0) {
      $this->quality--;
    }
    $this->sell_in--;
    if ($this->sell_in < 0 && $this->quality > 0) {
      $this->quality--;
    }
  }
}