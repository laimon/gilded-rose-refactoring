<?php

namespace App;

class ItemTemplate {
  public $name;
  public $sell_in;
  public $quality;

  function __construct(Item $item) {
    $this->name    = $item->name;
    $this->sell_in = $item->sell_in;
    $this->quality = $item->quality;
  }

  public function __toString() {
      return "{$this->name}, {$this->sell_in}, {$this->quality}";
  }

}
